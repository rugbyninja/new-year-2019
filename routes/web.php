<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('new-year');
});

Route::get('/iframe', function () {
    return view('new-year-2');
});


Route::get('/iframe-2', function () {
    return view('new-year-3');
});

Route::get('/iframe-3', function () {
    return view('new-year-4');
});
